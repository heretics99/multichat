from django.contrib import admin
from multichatapp.models import *

class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['id','user','region','date_of_birth','sex_type']
    search_fields = ['id','user__username']
    list_filter = ['region']
admin.site.register(UserProfile,UserProfileAdmin)

class MessageAdmin(admin.ModelAdmin):
    inline = [User]
    list_display = ['id','user','title','created_at','is_approved']
    search_fields = ['id','user__username','title','is_approved']
    list_filter = ['created_at','is_approved']
    filter_horizontal = ('members',)
#    fieldsets       = [
#           ('Fields', {'fields':['title','body','user','created_at','updated_at','is_approved','members']}),
#        ]

admin.site.register(Message,MessageAdmin)
    
#class MembershipInline(admin.TabularInline):
#    model = User

#class MembershipAdmin(admin.ModelAdmin):
#    list_display = ['id','user','message','status','first_read_date']
#    search_fields = ['id','user__username']
#    list_filter = ['status']
#admin.site.register(Membership,MembershipAdmin)
    