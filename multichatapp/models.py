from django.db import models
from django.contrib.contenttypes.models import ContentType
from datetime import datetime
from django.db.models.fields.related import ForeignKey
from django.contrib.contenttypes import generic
from django.contrib.auth.models import User
from django.conf import settings

REGIONS= (
        ('Ungrouped','Ungrouped'),
        ('AS','Asia'),
        ('EU','Europe'),
        ('NA','North America'),
        ('SA','South America'),
        ('AF','Africa'),
        ('AN','Antartica'),
        ('OC','Oceania'),
)

SEX= (
      ('M', 'Male'),
      ('F', 'Female')
)
class Region(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=50)
    def __unicode__(self):
        return u"%s"%self.name
    class Meta:
        db_table = "jc_regions"
    
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    region = models.ForeignKey(Region, null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    sex_type = models.CharField(max_length=10, choices=SEX, null=True, blank=True)
    profile_pic_url = models.CharField(max_length=255, null=True, blank=True)
    class Meta:
        db_table = 'jc_user_profiles'
    def __unicode__(self):
        return u"%s"%(self.user)

class Message(models.Model) :
    title = models.CharField(max_length=255,blank=True,null=True) 
    body = models.TextField(blank=True,null=True)
    user= models.ForeignKey(User)
    created_at = models.DateTimeField(blank=True, default = datetime.now)
    updated_at = models.DateTimeField(default = datetime.now, blank=True)
    is_approved = models.PositiveSmallIntegerField(default = 0)
    members = models.ManyToManyField(User, related_name="messagem2m_set")
    def __unicode__ (self) :
            return u"%s"%(self.title)
    class Meta:
            db_table='jc_message'
                
#class Membership(models.Model):
#    user = models.ForeignKey(User)
#    message = models.ForeignKey(Message)
#    status = models.IntegerField(default = 0, blank=True, null=True) #0-unread, 1-read, 2-flagged
#    first_read_date = models.DateTimeField(blank=True, null=True)
#    def __unicode__ (self) :
#            return u"%s-%s"%(self.user, self.message)
#    class Meta:
#            db_table='jc_message_members'
#                
