# Create your views here.
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.context_processors import csrf
from multichatapp.models import *

template = lambda name: 'multichatapp/%s'%name

def authentication(request):
    """ Does authentication and registration in case the user has signed up """
    next = request.REQUEST.get('next','/')
    if request.user.is_authenticated():
        return HttpResponseRedirect("/")
    email = password = ""
    wrong_password = False
    state = "Not authenticated"
    auth_user = None
    if request.POST:
        email = request.POST.get('email')
        password = request.POST.get('password')
        if '@' in email:
            kwargs = {'email': email}
        else:
            kwargs = {'username': email}
        if User.objects.filter(**kwargs).count():
            auth_user = authenticate(username=email, password=password)
            if auth_user is not None:
                login(request, auth_user)
                state = "Existing user. Logged In"
            else:
                wrong_password = True
                state = "Wrong Password"
                print email, password
        else:
            user = User.objects.create_user(email, email, password)
            user.is_staff = True
            user.save()
            auth_user = authenticate(username=user.email, password=user.password)
            if auth_user is not None:
                login(request, auth_user)
            state = "New User created. Logged In"
    print email, "|", password
    print auth_user
    print state
    return render_to_response('login.html',{'wrong_password':wrong_password}, RequestContext(request))
    
@login_required
def compose(request):
    """ Its the home view. Handles the composed message. Returns confirmation. Login is required."""
    data = {}
    regions = Region.objects.all()
    data['messages'] = Message.objects.filter(members = request.user)
    data['regions'] = regions
#    for r in regions:
#        r.userprofile_set = r.userprofile_set.all()
#    all_users = UserProfile.objects.all()
#    for u in all_users:
#        regions.setdefault(u.region,[]).append(u)
    if request.method == 'POST':
#        valid = False
        data['status'] = ""
        usernames = request.POST.getlist('usernames')
        subject = request.POST.get('subject')
        message = request.POST.get('message')
        if len(usernames)>0:
            if len(subject) > 0 and len(message)>0:
    #            valid = True
                msg_obj = Message(title=subject, body=message, user=request.user, is_approved=1)
                msg_obj.save()
                msg_obj.members.add(*[int(x) for x in usernames])
                data['status'] = "Your Message is posted Successfully."
            else:
                data['status'] = "Either Subject or Message is missing."
        else:
            data['status'] = "You need to put atleast one receipent before you can post."
        data['title'] = subject
        data['body'] = message
    return render_to_response('compose.html',{'data':data}, RequestContext(request))

@login_required
def inbox(request):
    """ Inbox view. Shows all messages sent to user."""
    data = {}
    data['messages'] = Message.objects.filter(members = request.user)
    return render_to_response('inbox.html',{'data':data}, RequestContext(request))

@login_required
def profile(request):
    """ Option to change user profile."""
    data = {}
    data['username'] = request.user.username
    data['regions'] = Region.objects.all()
    if request.method == 'POST':
        try:
            cu = UserProfile.objects.get(user=request.user)
        except:
            cu = UserProfile(user=request.user)
        new_region = request.POST.get('region')
        cu.region = Region.objects.get(id=int(new_region))
        cu.save()
        new_password = request.POST.get('password')
        print new_region, new_password
        if(len(new_password)>0):
            u = User.objects.get(pk=request.user.id)
            u.set_password(new_password)
            u.save() 
        data['status'] = "success"

    try:
        data['current_region'] = UserProfile.objects.get(user=request.user).region.id
    except:
        data['current_region'] = ""
    return render_to_response('profile.html',{'data':data}, RequestContext(request))

def logout_view(request):
    """Log users out and re-direct them to the main page."""
    logout(request)
    return HttpResponseRedirect('/')


def registration(request):
    data = {}
    data['regions'] = Region.objects.all()
    if request.method == 'POST':
        username = request.POST.get('username')
        region = request.POST.get('region')
        password = request.POST.get('password')
        password_repeat = request.POST.get('password_repeat')
        if len(username) > 0 and len(password) > 0:
            if password_repeat == password:
                try:
                    user = User.objects.create_user(username, email="", password=password)
                    try:
                        cu = UserProfile(user=user, region_id = region)
                        cu.save()
                        auth_user = authenticate(username=username, password=password)
                        if auth_user is not None:
                            login(request, auth_user)
                        return HttpResponseRedirect('/')
                    except Exception as e:
                        data['status'] = "User Profile not created"
                except:
                    data['status'] = "You're Late, that Username is already taken!"
            else:
                  data['status'] = "Easy, your passwords dont match."
        else:
            data['status'] = "Well, you cant keep your username or password blank, because we cant read your mind (yet)."
    return render_to_response('registration/registration.html', {'data':data}, RequestContext(request))

