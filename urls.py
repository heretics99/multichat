from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import redirect_to
from django.conf import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'multichatapp.views.compose', name='compose'),
    url(r'^compose/', 'multichatapp.views.compose', name='compose'),
    url(r'^inbox/', 'multichatapp.views.inbox', name='inbox'),
    url(r'^profile/', 'multichatapp.views.profile', name='profile'),
    url(r'^accounts/profile/', redirect_to, {'url': '/'}),
    url(r'^signup/', 'multichatapp.views.registration', name='signup'),
    url(r'^accounts/login/', 'django.contrib.auth.views.login', name='authentication'),
    url(r'^accounts/logout/', 'multichatapp.views.logout_view', name='logout'),
          
    # url(r'^multichat/', include('multichat.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
#     (r'static/(?P<path>.*)$'), 'django.views.static.serve', {'document_root':settings.STATIC_ROOT}
)
